package persistent.com.movemploy.app

import android.app.Application
import com.facebook.stetho.Stetho
import persistent.com.movemploy.BuildConfig
import persistent.com.movemploy.R
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyConfig

/**
 * Created by josedura-c on 2/23/18.
 */
class App : Application() {

  override fun onCreate() {
    super.onCreate()

    if (BuildConfig.DEBUG) {
      Stetho.initializeWithDefaults(this)
      Timber.plant(Timber.DebugTree())
    }

    CalligraphyConfig.initDefault(
        CalligraphyConfig.Builder()
            .setDefaultFontPath("fonts/Oswald-Medium.ttf")
            .setFontAttrId(R.attr.fontPath)
            .build())


  }

}