package persistent.com.movemploy.view.generic.sms_validation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.telephony.SmsMessage
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding2.widget.RxTextView
import kotlinx.android.synthetic.main.activity_sms_validation.*
import persistent.com.movemploy.R
import persistent.com.movemploy.protocol.ApiUtils
import persistent.com.movemploy.utils.SharedPreferencesUtils
import persistent.com.movemploy.view.bus_driver.start_route.StartRouteActivity
import persistent.com.movemploy.view.user.take_profile_photo.TakeProfileActivity
import timber.log.Timber
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper


class SmsValidationActivity : AppCompatActivity() {

  //region Global Variables
  private var presenter: SmsValidationPresenter? = null
  //endregion

  //region Activity Methods
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_sms_validation)

    presenter = SmsValidationPresenter(this@SmsValidationActivity)

    initViews()
  }

  override fun attachBaseContext(newBase: Context) {
    super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
  }

  override fun onResume() {
    super.onResume()

    val filter = IntentFilter("android.provider.Telephony.SMS_RECEIVED")

    val mReceiver = object : BroadcastReceiver() {
      override fun onReceive(context: Context, intent: Intent) {
        val bundle = intent.extras
        val pdusObj = bundle.get("pdus") as Array<Any>

        for (i in pdusObj.indices) {

          val currentMessage = SmsMessage.createFromPdu(pdusObj[i] as ByteArray)
          val phoneNumber = currentMessage.getDisplayOriginatingAddress()

          val message = currentMessage.getDisplayMessageBody()

          val words = message.split(" ")
          try {
            val number = words[words.size - 1].toInt()
            Timber.e("code number -> $number")
            activity_sms_validation_pinView.setText("$number")
          } catch (e: NumberFormatException) {
            Timber.e("the message have not code number")
          }
        } // end for loop
      }
    }
    registerReceiver(mReceiver, filter)
  }
  //endregion

  //region local methods
  private fun initViews() {
    RxTextView.textChanges(activity_sms_validation_pinView)
        .map { it.toString() }
        .map { it.length }
        .subscribe {
          if (it == 4) {
            registerSmsCode()
          }
        }
  }

  private fun registerSmsCode() {
    presenter?.registerSmsCode(
        SharedPreferencesUtils.getSharedPreferences(this@SmsValidationActivity)
            .getString(SharedPreferencesUtils.PHONE_NUMBER, ""),
        activity_sms_validation_pinView.text.toString(),
        onSuccess = {
          when (it) {
            ApiUtils.NORMAL_USER -> {
              startActivity(Intent(this@SmsValidationActivity, TakeProfileActivity::class.java))
            }
            ApiUtils.BUS_DRIVER_USER -> {
              startActivity(Intent(this@SmsValidationActivity, StartRouteActivity::class.java))
            }
          }
        },
        onFail = {
          onRegisterNumberFail(it)
        }
    )
  }

  private fun onRegisterNumberFail(errorCode: String) {
    MaterialDialog.Builder(this@SmsValidationActivity)
        .title(R.string.activity_sms_validation_error_title)
        .content(errorCode)
        .icon(ContextCompat.getDrawable(this@SmsValidationActivity, R.drawable.ic_warning)!!)
        .positiveText(R.string.retry_request)
        .onPositive({ _, _ -> registerSmsCode() })
        .negativeText(R.string.ok_request)
        .cancelable(false)
        .canceledOnTouchOutside(false)
        .show()

  }
  //endregion

}
