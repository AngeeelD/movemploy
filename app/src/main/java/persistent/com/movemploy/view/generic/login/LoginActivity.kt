package persistent.com.movemploy.view.generic.login

import android.Manifest
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import androidx.content.edit
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding2.widget.RxTextView
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_login.*
import persistent.com.movemploy.R
import persistent.com.movemploy.protocol.entities.login.LoginRequest
import persistent.com.movemploy.utils.SharedPreferencesUtils
import persistent.com.movemploy.view.generic.sms_validation.SmsValidationActivity



class LoginActivity : AppCompatActivity() {

  //region Global Variables
  private var presenter: LoginPresenter? = null
  //endregion

  //region Activity Methods
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_login)

    presenter = LoginPresenter(this@LoginActivity)

    initViews()
  }

  override fun onResume() {
    super.onResume()
  }
  //endregion

  //region Local Methods
  private fun initViews() {
    RxTextView.textChanges(activity_loging_phone_number)
        .map { it.toString() }
        .filter { it.length == 10 }
        .subscribe({
          activity_login_login_button.isEnabled = true
        })
    activity_login_login_button.setOnClickListener {
      RxPermissions(this@LoginActivity)
          .requestEach(Manifest.permission.RECEIVE_SMS)
          .subscribe({
            when {
              it.granted -> {
                registerNumber()
              }
              it.shouldShowRequestPermissionRationale -> {
                registerNumber()
              }
              else -> {
                MaterialDialog.Builder(this@LoginActivity)
                    .title(R.string.main_fragment_select_image_action_message_title)
                    .title(R.string.main_fragment_select_image_action_message_content)
                    .positiveText(R.string.main_fragment_select_image_action_message_positive_action)
                    .onPositive({ _, _ ->
                      val i = Intent()
                      i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                      i.addCategory(Intent.CATEGORY_DEFAULT)
                      i.data = Uri.parse("package:" + this@LoginActivity.packageName)
                      i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                      i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                      i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                      this.startActivity(i)
                    })
                    .show()
              }
            }
          })
    }
  }

  private fun registerNumber() {
    presenter?.loginWithNumber(
        LoginRequest(activity_loging_phone_number.text.toString()),
        onSuccess = {
          SharedPreferencesUtils.getSharedPreferences(this@LoginActivity).edit {
            putString(SharedPreferencesUtils.PHONE_NUMBER, activity_loging_phone_number.text.toString())
          }
          onRegisterNumberSuccess()
        }, onFail = {
      onRegisterNumberFail(it)
    })
  }

  private fun onRegisterNumberSuccess() {
    startActivity(Intent(this@LoginActivity, SmsValidationActivity::class.java))
  }

  private fun onRegisterNumberFail(errorCode: String) {
    MaterialDialog.Builder(this@LoginActivity)
        .title(R.string.activity_sms_validation_error_title)
        .content(errorCode)
        .icon(ContextCompat.getDrawable(this@LoginActivity, R.drawable.ic_warning)!!)
        .positiveText(R.string.retry_request)
        .onPositive({ _, _ -> registerNumber() })
        .negativeText(R.string.ok_request)
        .cancelable(false)
        .canceledOnTouchOutside(false)
        .show()

  }
  //endregion



}
