package persistent.com.movemploy.view.generic.sms_validation

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import persistent.com.movemploy.protocol.api.RiteApiClient
import persistent.com.movemploy.protocol.entities.register.SmsRegisterRequest
import persistent.com.movemploy.protocol.service.RiteServiceGenerator
import persistent.com.movemploy.utils.ConnectionsUtils
import retrofit2.HttpException
import timber.log.Timber

/**
 * Created by josedura-c on 2/24/18.
 */
class SmsValidationPresenter(val context: Context) {

  //region Global Variables
  fun registerSmsCode(phoneNumber: String,
                      smsCode: String,
                      onSuccess: ((String) -> Unit)? = null,
                      onFail: ((String) -> Unit)? = null) {
    val smsRegisterRequest = SmsRegisterRequest(phoneNumber, smsCode)

    RiteServiceGenerator()
        .createService(RiteApiClient::class.java)
        .registerSmsCode(smsRegisterRequest)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          if (it.status == 200) {
            onSuccess?.invoke(it.role)
          } else {
            onFail?.invoke(it.data)
          }
        }, {
          Timber.e(it)
          if (it is HttpException) {
            onFail?.invoke(ConnectionsUtils.getErrorMessage(context, it.code()))
          } else {
            onFail?.invoke(ConnectionsUtils.getErrorMessage(context, ConnectionsUtils.getErrorCode(it)))
          }
        })
  }
  //endregion

}

