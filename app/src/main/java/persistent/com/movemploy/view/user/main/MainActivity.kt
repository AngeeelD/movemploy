package persistent.com.movemploy.view.user.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import persistent.com.movemploy.R

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
  }
}
