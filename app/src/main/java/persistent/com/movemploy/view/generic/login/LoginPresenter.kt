package persistent.com.movemploy.view.generic.login

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import persistent.com.movemploy.protocol.api.RiteApiClient
import persistent.com.movemploy.protocol.entities.login.LoginRequest
import persistent.com.movemploy.protocol.service.RiteServiceGenerator
import persistent.com.movemploy.utils.ConnectionsUtils
import retrofit2.HttpException
import timber.log.Timber

/**
 * Created by josedura-c on 2/24/18.
 */
class LoginPresenter(val context: Context) {

  //region Global Variables

  //endregion

  //region Local Methods
  fun loginWithNumber(loginRequest: LoginRequest,
                      onSuccess: (() -> Unit)? = null,
                      onFail: ((String) -> Unit)? = null) {
    RiteServiceGenerator()
        .createService(RiteApiClient::class.java)
        .login(loginRequest)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({
          if (it.status == 200) {
            onSuccess?.invoke()
          } else {
            onFail?.invoke(it.data)
          }
        }, {
          Timber.e(it)
          if (it is HttpException) {
            onFail?.invoke(ConnectionsUtils.getErrorMessage(context, it.code()))
          } else {
            onFail?.invoke(ConnectionsUtils.getErrorMessage(context, ConnectionsUtils.getErrorCode(it)))
          }
        })
  }
  //endregion

}