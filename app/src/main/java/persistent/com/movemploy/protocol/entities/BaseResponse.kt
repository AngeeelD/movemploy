package persistent.com.movemploy.protocol.entities

import com.google.gson.annotations.SerializedName

/**
 * Created by josedura-c on 2/24/18.
 */
open class BaseResponse(@SerializedName("status") var status: Int = 500)