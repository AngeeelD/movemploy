package persistent.com.movemploy.protocol.service

import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import persistent.com.movemploy.BuildConfig
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
 * Created by josedura-c on 2/24/18.
 */
class RiteServiceGenerator {

  var apiBaseUrl = BuildConfig.BASE_API_URL

  private val retrofit: Retrofit? = null

  private val rxAdapter = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

  private var builder: Retrofit.Builder = Retrofit.Builder()
      .addConverterFactory(GsonConverterFactory.create(
          GsonBuilder()
              .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
              .create()))
      .addConverterFactory(ScalarsConverterFactory.create())
      .addCallAdapterFactory(rxAdapter)
      .baseUrl(apiBaseUrl)

  private val httpClient = OkHttpClient.Builder()
      .addInterceptor(HttpLoggingInterceptor()
          .setLevel(HttpLoggingInterceptor.Level.BODY))

  fun changeApiBaseUrl(newApiBaseUrl: String) {
    apiBaseUrl = newApiBaseUrl

    builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(
            GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create()
        ))
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(apiBaseUrl)
  }

  fun <S> createService(serviceClass: Class<S>): S {
    val retrofit = builder.client(httpClient.build()).build()
    return retrofit.create(serviceClass)
  }

}