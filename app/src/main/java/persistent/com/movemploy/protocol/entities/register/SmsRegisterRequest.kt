package persistent.com.movemploy.protocol.entities.register

import com.google.gson.annotations.SerializedName

/**
 * Created by josedura-c on 2/24/18.
 */
class SmsRegisterRequest(@SerializedName("number") val number: String,
                         @SerializedName("pin") val pin: String)