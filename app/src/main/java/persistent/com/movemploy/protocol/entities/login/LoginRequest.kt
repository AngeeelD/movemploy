package persistent.com.movemploy.protocol.entities.login

import com.google.gson.annotations.SerializedName

/**
 * Created by josedura-c on 2/24/18.
 */
data class LoginRequest(private @SerializedName("number") val number: String)