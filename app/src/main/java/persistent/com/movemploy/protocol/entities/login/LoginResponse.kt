package persistent.com.movemploy.protocol.entities.login

import com.google.gson.annotations.SerializedName
import persistent.com.movemploy.protocol.entities.BaseResponse

/**
 * Created by josedura-c on 2/24/18.
 */
class LoginResponse(@SerializedName("data") val data: String) : BaseResponse()