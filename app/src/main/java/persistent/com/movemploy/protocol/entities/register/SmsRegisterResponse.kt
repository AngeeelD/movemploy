package persistent.com.movemploy.protocol.entities.register

import com.google.gson.annotations.SerializedName
import persistent.com.movemploy.protocol.entities.BaseResponse

/**
 * Created by josedura-c on 2/24/18.
 */
class SmsRegisterResponse(@SerializedName("role") val role: String,
                          @SerializedName("data") val data: String) : BaseResponse()