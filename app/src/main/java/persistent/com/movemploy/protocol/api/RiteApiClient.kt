package persistent.com.movemploy.protocol.api

import io.reactivex.Observable
import persistent.com.movemploy.protocol.entities.BaseResponse
import persistent.com.movemploy.protocol.entities.login.LoginRequest
import persistent.com.movemploy.protocol.entities.login.LoginResponse
import persistent.com.movemploy.protocol.entities.register.SmsRegisterRequest
import persistent.com.movemploy.protocol.entities.register.SmsRegisterResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

/**
 * Created by josedura-c on 2/24/18.
 */
interface RiteApiClient {

  //region Public EndPoints
  @POST("/login")
  fun login(@Body loginRequest: LoginRequest): Observable<LoginResponse>

  @POST("/confirmLogin")
  fun registerSmsCode(@Body smsRegisterRequest: SmsRegisterRequest): Observable<SmsRegisterResponse>

  @GET("/ppartida")
  fun pickup(): Observable<BaseResponse>

  @GET("/confirmar_ruta")
  fun confirmRoute(): Observable<BaseResponse>

  /*
  @GET("/v3/ticker/")
  fun getTicker(
      @Query("book") book: String? = null //Specifies which book to use
  ): Observable<TickerResponse>

  @GET("/v3/order_book/")
  fun getOrderBooks(@Query("book") book: String, //Specifies which book to use
                    @Query("aggregate") aggregate: Boolean? = null // Specifies if orders should be aggregated by price.
  ): Observable<OrderBookResponse>

  @GET("/v3/trades/")
  fun getTrades(@Query("book") book: String, // Specifies which book to use
                @Query("marker") marker: String? = null, // Returns objects that are older or newer (depending on 'sort’) than the object with this ID
                @Query("sort") sort: String? = null, // Specifies ordering direction of returned objects ('asc’, 'desc’)
                @Query("limit") limit: Int? = null): Observable<TradeResponse> // Specifies number of objects to return. (Max is 100)
  //endregion

  //region Private EndPoints
  @GET("/v3/balance")
  fun getBalance(@Header("Authorization") Authorization: String = BitsoManager.getAuthorizationHeaderValue()): Observable<BalanceResponse>

  fun getAccountStatus()
  //endregion

  */
}