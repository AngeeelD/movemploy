package persistent.com.movemploy.utils

import android.content.Context

/**
 * Created by josedura-c on 2/24/18.
 */
class SharedPreferencesUtils {

  companion object {

    val PHONE_NUMBER = "PHONE_NUMBER"
    val B_POOL = "B_POOL"

    fun getSharedPreferences(context: Context) = context.getSharedPreferences(B_POOL, Context.MODE_PRIVATE)

  }

}