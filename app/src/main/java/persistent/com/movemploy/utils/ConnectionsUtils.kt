package persistent.com.movemploy.utils

import android.content.Context
import persistent.com.movemploy.R
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

/**
 * Created by josedura-c on 2/24/18.
 */
class ConnectionsUtils {

  companion object {

    fun getErrorCode(throwable: Throwable): Int =
        if (throwable is SocketTimeoutException) {
          1
        } else if (throwable is UnknownHostException || throwable is ConnectException) {
          2
        } else {
          -1
        }

    fun getErrorMessage(context: Context, errorCode: Int)
        : String = context.getString(when (errorCode) {
      400 -> {
        R.string.error_code_400
      }
      401 -> {
        R.string.error_code_401
      }
      402 -> {
        R.string.error_code_402
      }
      403 -> {
        R.string.error_code_403
      }
      404 -> {
        R.string.error_code_404
      }
      405 -> {
        R.string.error_code_405
      }
      406 -> {
        R.string.error_code_406
      }
      407 -> {
        R.string.error_code_407
      }
      408 -> {
        R.string.error_code_408
      }
      409 -> {
        R.string.error_code_409
      }
      500 -> {
        R.string.error_code_500
      }
      else -> {
        R.string.error_code_500
      }
    })

  }

}